
$(document).ready(function(){
    // Setting target donation
    targetDonation = 300;

    // Updating Remaining Days
    updateReminingDays();

    // Setting up Donors Count
    updateDonorCount();

    // Setting up donated Amount and Progress bar
    updateDonatedAmount();

    // Validation For Donations
    var maxDonatableAmount;
    $('.donForm > form').submit(function(e) {
        donatedAmount = parseFloat(getPrevDonatedAmount());
        maxDonatableAmount = targetDonation - donatedAmount;

        // Update Validation Rules
        updateRule(maxDonatableAmount);

        e.preventDefault();

    }).validate({
        rules: {
            donAmount: {
                required: true,
                max: maxDonatableAmount,
                min: 1,
            }
        },
        messages: {
            donAmount: {
                required: "Please enter an Amount!",
                max: "Please enter an amount less than or equal to $" + maxDonatableAmount,
                min: "Please enter an amount greater than or equal to $1",
            }
        },
        submitHandler: function(form) {
            // Update Donated Amount And Progress
            updateDonatedAmount(true);

            // incrementing Donor Count
            updateDonorCount(true);

            // Check If Target Reached on the GO
            showDonTargetReachedMsg();

            // Predict Next Donation
            suggestNextDonationAmount();

            return false;
        }
    });

    // Check If target donation reached
    showDonTargetReachedMsg();

    // Predict Next Donation
    suggestNextDonationAmount();

});

// Update Rules For validation after donation
function updateRule(maxDonatableAmount){
    $( "#donAmount" ).rules( "add", {
        required: true,
        max: maxDonatableAmount,
        messages: {
            required: "Please enter an Amount!",
            max: "Please enter an amount less than or equal to " + maxDonatableAmount,
        }
    });
}

// Storing the Donor Number to Session
function updateDonorCount(increment = false){
    if (window.sessionStorage) {
        var donorsCount = sessionStorage.getItem( 'donors' );

        if (donorsCount && increment ) {
            donorsCount = parseInt(donorsCount);
            sessionStorage.setItem( 'donors', ++donorsCount );
        }else if (donorsCount && !increment ) {
            sessionStorage.setItem( 'donors', donorsCount );
        } else{
            donorsCount = 0;
            sessionStorage.setItem( 'donors', donorsCount );
        }

        $('.donorCount').text( donorsCount );
    }
}

// Storing the Donor Number to Session
function updateDonatedAmount(justDonated = false){
    if (window.sessionStorage) {
        var donationAmount = parseFloat($('input#donAmount').val());
        var donatedAmount = parseFloat(getPrevDonatedAmount());

        if(justDonated){
            // On successfull donation
            donatedAmount = donationAmount + donatedAmount;
            $('.remainingBudget span').text( targetDonation - donatedAmount );
            sessionStorage.setItem( 'donatedAmount', Math.round(donatedAmount) );
        }else{
            // Updating Progress with Previous Donation
            $('.remainingBudget span').text( targetDonation - donatedAmount );
        }
        
        // Progress Bar
        let barPercent = donatedAmount / targetDonation * 100 ;
        $('.progressBar').css('width', barPercent + '%');

    }
}

// Get Previously Donated Amount
function getPrevDonatedAmount(){
    var donationInSession = sessionStorage.getItem( 'donatedAmount' );
    if (donationInSession) {
        return donationInSession;
    }else{
        return '0';
    }
}

// Update Remaining days Function
function updateReminingDays(){
    // April 20, 2019
    let lastDate = "2019-04-20";
    parseInt($('.donDaysRemaining').text())

    let start = new Date(lastDate),
    end   = new Date(),
    diff  = new Date( start - end),
    days  = diff/1000/60/60/24;

    $('.donDaysRemaining').text(Math.floor(days));
}

// Show success message if target donation reached
function showDonTargetReachedMsg(){
    if (getPrevDonatedAmount() >= targetDonation) {
        $('.remainingBudget').text('Target Donation Amount Reached');

        $('.donationContainer').fadeOut(800);
        window.setTimeout(function () {
            $(".donTargetReachedContainer").fadeIn(100);
        }, 801);
    }
}

// Predict Next Donation Amount Automatically
function suggestNextDonationAmount(){
    donatedAmount = parseFloat(getPrevDonatedAmount());
    maxDonatableAmount = targetDonation - donatedAmount;

    if (
        maxDonatableAmount < parseInt($('input#donAmount').val()) 
        || maxDonatableAmount < 50
        || (maxDonatableAmount >=  50 && parseInt($('input#donAmount').val()) < 50 )
        ) {

        $('input#donAmount').val(maxDonatableAmount > 50 ? 50: maxDonatableAmount);
    }
}

// Reset Donation sessions and counters
function resetDonationSessions(){
    sessionStorage.clear();
    window.location.reload();
}